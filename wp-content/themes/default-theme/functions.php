<?php
/**
 * MelanieC functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package MelanieC
 */

if ( ! function_exists( 'default_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function default_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on MelanieC, use a find and replace
	 * to change 'default-theme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'default-theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'default-theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'default_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'default_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function default_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'default_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'default_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function default_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'default-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'default-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'default_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function default_theme_scripts() {
	wp_enqueue_style( 'default-theme-style', get_stylesheet_uri() );

	wp_enqueue_script( 'default-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	
	wp_enqueue_script( 'default-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
		
	wp_enqueue_script( 'default-theme-jquery-ui', get_template_directory_uri() . '/fullpage-js/jquery-ui.min.js', array(), array(), true );
	
	//wp_enqueue_script( 'default-theme-overflow', get_template_directory_uri() . '/fullpage-js/scrolloverflow.js', array(), array(), true );
		
	wp_enqueue_script( 'default-theme-jquery-fullpage', get_template_directory_uri() . '/fullpage-js/jquery.fullPage.min.js', array(), array(), true );

	wp_enqueue_script( 'default-theme-fullpage', get_template_directory_uri() . '/fullpage-js/fullpage.js', array(), array(), true );

	wp_enqueue_script( 'default-jquery', get_template_directory_uri() . '/js/jquery.min.js');

	wp_enqueue_script( 'default-bootstrap-js', get_template_directory_uri() . '/bootstrap-3.3.7-dist/js/bootstrap.min.js');

	wp_enqueue_style( 'default-bootstrap-css', get_template_directory_uri() . '/bootstrap-3.3.7-dist/css/bootstrap.min.css');

	wp_enqueue_style( 'default-custom-css', get_template_directory_uri() . '/custom-style.css');
	
	wp_enqueue_style( 'default-fullpage-css', get_template_directory_uri() . '/fullpage-js/jquery.fullPage.css');
	
	wp_enqueue_style( 'default-Oswald-font', '//fonts.googleapis.com/css?family=Oswald:300,400');
	
	wp_enqueue_style( 'default-Montserrat-font', '//fonts.googleapis.com/css?family=Montserrat:400');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'default_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// Custom WP Admin footer
function remove_footer_admin () {

  echo 'Fueled by <a href="http://www.wordpress.org" target="_blank">WordPress</a> | Developer: <a href="//www.ayrah.com/" target="_blank">Patricia Ayrah R. Otero</a></p>';

}
add_filter('admin_footer_text', 'remove_footer_admin');
