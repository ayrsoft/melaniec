<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MelanieC
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'default-theme' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding none">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation none" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'default-theme' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">

	<?php if(! is_front_page()):?>
		<div class="container-logo body-color row  section fp-auto-height " >
	        <div class="col-md-12 mycontent-logo" id="live-logo">
				<a href="/"><img src="/wp-content/uploads/2017/04/melaniec-logo.png" class="img-responsive" alt="melaniec" /></a>
				<ul class="list-inline text-center icons" >
					<li><a href="http://bit.ly/MelanieC_Twitter" target="_blank" title="Twitter"><img src="/wp-content/themes/default-theme/img/icons/twitter.png" alt="Twitter""></a></li>
					<li><a href="http://bit.ly/MelanieC_Instagram" target="_blank" title="Instagram"><img src="/wp-content/themes/default-theme/img/icons/instagram.png" alt="Instagram""></a></li>
					<li><a href="http://bit.ly/MelanieC_Facebook" target="_blank" title="Facebook"><img src="/wp-content/themes/default-theme/img/icons/fb.png" alt="Facebook""></a></li>
					<li><a href="http://bit.ly/MelanieC_Youtube" target="_blank" title="YouTube"><img src="/wp-content/themes/default-theme/img/icons/yt.png" alt="YouTube""></a></li>
					<li><a href="http://bit.ly/MelanieC_iTunes" target="_blank" title="iTunes"><img src="/wp-content/themes/default-theme/img/icons/itunes.png" alt="iTunes""></a></li>
					<li><a href="http://bit.ly/MelanieC_Spotify" target="_blank" title="Spotify"><img src="/wp-content/themes/default-theme/img/icons/spotify.png" alt="Spotify""></a></li>
					<li><a href="/news" target="_self" title="Newsletter"><img src="/wp-content/themes/default-theme/img/icons/email.png" alt="newsletter""></a></li>
					<li><a href="//store.melaniec.net" target="_blank" title="Store"><img src="/wp-content/themes/default-theme/img/icons/cart.png" alt="Store""></a></li>
				</ul>
			</div>
	    </div>
	<?php endif; ?>
