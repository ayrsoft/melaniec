<?php
/**
 * Template Name: Live
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>
	<div class="section body-color container" id="sec-live">
			<?php 
			$paged = max(get_query_var('paged'),1);
			$args  = array('post_type' => 'concert');
			
			$query = new WP_Query($args);

			while($query->have_posts()) :  
			$query->the_post();
			?>
			<div class="row">
				<div class='col-md-12 text-center live-title'>
					<h1>LIVE <?php echo the_title() ?></h1>
				</div>
			</div>

			<div class="row m-r-20 m-l-20 align-text-l">
			<?php
					$concert_details = get_field('concert_details');

					if( have_rows('concert_details') ):

						while( have_rows('concert_details') ): the_row(); 
			?>
							<div class="col-md-3 col-sm-4 col-xs-12 " id="live-page">
								<h3 class="concert-place"><span class="text-u text-dark-blue"><?php the_sub_field('place'); ?></span></h3>
						<?php 						
								while( have_rows('concert') ): the_row();
						?>
									<h3 class="concert-place" id="concert-month"><span class="text-u text-dark-blue"><br></span><span class="text-light-blue text-u"><?php the_sub_field('month'); ?></span></h3>	
									<br>
						<?php
										while( have_rows('concert_name') ): the_row();
						?>
											<p><a href="<?php the_sub_field('link'); ?>"><span class="text-light-blue"><?php the_sub_field('concert_title'); ?></span></a></p>			
						<?php
										endwhile;

								endwhile;

							
						?>	
							</div>

						<?php
				
						endwhile;

					endif;

			endwhile;
				
				wp_reset_postdata();

	    		?>

			</div>
</div>		
<?php 
get_footer();
