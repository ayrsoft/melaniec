<?php
/**
 * Template Name: Lyrics
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>
		<div class="section body-color container" id="sec-lyrics">
			<div class="row">
				<?php 
				$paged = max(get_query_var('paged'),1);
				$args  = array(
					'post_type' => 'album'
				);

				$query = new WP_Query($args);

				while($query->have_posts()) :  
				$query->the_post();
	    		?>
				<?php 
					$status = get_field('active_flag');
					$music  = get_field('music');
					if( $music ): 
				?>
				<?php 	if($status[0] == "active") { ?>
							<div class="col-md-12 text-center text-dark-blue">
								<h1  id="album-title">{ <?php the_title();?> }</h1>
							</div>
							<div class="col-md-12 text-center text-dark-blue">
								<div class="desc">
									<div class="panel-group" id="accordion">
									<?php foreach($music as $key => $row): ?>
										<div class="panel">
									        <h2 id="song-num">
									          	<?php echo $key+1 ?><a data-toggle="collapse" data-parent="#accordion" href="<?php echo '#collapse'.$key?>"><?php echo  " ".$row['song-title']; ?></a>
									        </h2>
									      	<div id="<?php echo 'collapse'.$key?>" class="panel-collapse collapse">
										        <div class="panel-body">
													<p><?php echo $row['lyrics']; ?></p>
										        </div>
									     	</div>
									    </div>
									<?php endforeach; ?>
									</div>
								</div>
							</div>
						<?php }?>
						
					<?php endif; ?>
				<?php 	
				endwhile;
				wp_reset_postdata();
				?>
							
			</div>
		</div>
			
		
<?php 
get_footer();
