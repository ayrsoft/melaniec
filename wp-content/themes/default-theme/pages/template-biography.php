<?php
/**
 * Template Name: Biography
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MelanieC
 *
 */


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post();?>

				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div id="contact-page">
								<p><?php the_content(); ?></p>
							</div>
						</div>
					</div>
				</div>

			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer();
