<?php
/**
 * Template Name: Homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>
	
	<div id="fullpage"  >
		<div class="container-logo body-color row  section fp-auto-height" id="section0">
	        <div class="col-md-12 mycontent-logo">
				<a href="/"><img src="/wp-content/uploads/2017/04/melaniec-logo.png" class="img-responsive" alt="melaniec" /></a>
				<ul class="list-inline text-center icons">
					<li><a href="http://bit.ly/MelanieC_Twitter" target="_blank" title="Twitter"><img src="/wp-content/themes/default-theme/img/icons/twitter.png" alt="Twitter""></a></li>
					<li><a href="http://bit.ly/MelanieC_Instagram" target="_blank" title="Instagram"><img src="/wp-content/themes/default-theme/img/icons/instagram.png" alt="Instagram""></a></li>
					<li><a href="http://bit.ly/MelanieC_Facebook" target="_blank" title="Facebook"><img src="/wp-content/themes/default-theme/img/icons/fb.png" alt="Facebook""></a></li>
					<li><a href="http://bit.ly/MelanieC_Youtube" target="_blank" title="YouTube"><img src="/wp-content/themes/default-theme/img/icons/yt.png" alt="YouTube""></a></li>
					<li><a href="http://bit.ly/MelanieC_iTunes" target="_blank" title="iTunes"><img src="/wp-content/themes/default-theme/img/icons/itunes.png" alt="iTunes""></a></li>
					<li><a href="http://bit.ly/MelanieC_Spotify" target="_blank" title="Spotify"><img src="/wp-content/themes/default-theme/img/icons/spotify.png" alt="Spotify""></a></li>
					<li><a href="/news" target="_self" title="Newsletter"><img src="/wp-content/themes/default-theme/img/icons/email.png" alt="newsletter""></a></li>
					<li><a href="//store.melaniec.net" target="_blank" title="Store"><img src="/wp-content/themes/default-theme/img/icons/cart.png" alt="Store""></a></li>
				</ul>
			</div>
	    </div>
	    <div class="section" id="section1" style="background-image: url('<?php echo content_url( 'uploads/2017/04/outnow.jpg' ); ?>')">
			<div class="container cl-section3">
				<div class="row">
					<div class="col-md-12">
						<img src="/wp-content/uploads/2017/04/melaniec-version-of-me.png" class="img-responsive m-auto" alt="version-of-me"/>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center light-text ">
						<h1> { OUT NOW }</h1>
						<h2 class="align-text-c""><span class="album-title-color">&gt; <span class="text-u"><a href="//melaniec.lnk.to/VersionOfMe" target="_blank" class="album-title-color">buy now</a></span> &lt;<a href="https://youtu.be/vK8b7uXm8ss" target="_blank" class="album-title-color"><br></a>&nbsp;</span></h2>
					</div>
				</div>
			</div>		
		</div>
		<div class="section" id="section2" style="background-image: url('<?php echo content_url( 'uploads/2017/04/ticket.jpg' ); ?>')">
			<div class="container" id="div-tkt">
				<div class="row">
					<table id="tbl-tkt">
						<tbody>
							<tr>
								<td class="text-center"><h1><strong><span class="text-dark-blue">UK / IRELAND</span></strong><br><strong><span class="text-dark-blue">APRIL&nbsp;</span></strong></h1></td>
								<td class="text-center"  rowspan="3" width="10%">
									<h1 class="m-120"></h1>
									<h1 class="title-section">LIVE</h1>
									<h1 class="subtitle-section">2017</h1>
								</td>
								<td class="text-center" ><h1><strong><span class="text-dark-blue">GERMANY</span></strong><br><strong><span class="text-dark-blue">MAY&nbsp;</span></strong></h1></td>
							</tr>
							<tr>
								<td class="text-center" ><h1><strong><span class="text-dark-blue"><br>SWITZERLAND</span></strong><br><strong><span class="text-dark-blue">MAY</span></strong></h1></td>
								<td class="text-center" ><h1><strong><span class="text-dark-blue"><br>ISLE OF WIGHT</span></strong><br><strong><span class="text-dark-blue">FESTIVAL</span></strong></h1></td>
							</tr>
							<tr>
								<td class="text-center" ><h1><strong><span class="text-dark-blue"><br>&nbsp;DENMARK</span></strong><br><strong><span class="text-dark-blue">APRIL / JUNE</span></strong></h1></td>
								<td class="text-center" ><h1><strong><span class="text-dark-blue"><br>CARFEST</span></strong><br><strong><span class="text-dark-blue">JULY / AUGUST</span></strong></h1></td>
							</tr>
						</tbody>
					</table>
					<div  id="col-tkt" class="col-md-12 text-center">
						<h1 class="m-120"></h1>
						<h1 class="title-section">LIVE</h1>
						<h1 class="subtitle-section">2017</h1>
					</div>
					<div class="col-md-12 text-center light-text font" id="col-ticket">
						<a href="/live"><p id="ticket">} <span id="get-ticket">GET YOU TKTS NOW</span> { </p></a>
					</div>
				</div>
			</div>	
		</div>
		<div class="section" id="section3" style="background-image: url('<?php echo content_url( 'uploads/2017/04/dear-life.jpg' ); ?>')">
			<div class="container cl-section3">
				<div class="row">
					<div class="col-md-12 text-center light-text ">
						<h1> { DEAR LIFE }</h1>
						<h2 class="align-text-c""><span class="album-title-color">&gt; <span class="text-u"><a href="https://www.youtube.com/watch?v=CoqeTtdhXEE" target="_blank" class="album-title-color">play music video</a></span> &lt;<a href="https://youtu.be/vK8b7uXm8ss" target="_blank" class="album-title-color"><br></a>&nbsp;</span></h2>
					</div>
				</div>
			</div>		
		</div>
		<div class="section" id="section4" style="background-image: url('<?php echo content_url( 'uploads/2017/04/anymore.jpg' ); ?>')">
			<div class="container cl-section4">
				<div class="row">
					<div class="col-md-12 text-center light-text ">
						<h1> { ANYMORE }</h1>
						<h2 class="align-text-c""><span class="album-title-color">&gt; <span class="text-u"><a href="https://youtu.be/vK8b7uXm8ss" target="_blank" class="album-title-color">play music video</a></span> &lt;<a href="https://youtu.be/vK8b7uXm8ss" target="_blank" class="album-title-color"><br></a>&nbsp;</span></h2>
					</div>
				</div>
			</div>		
		</div>
		<div class="section" id="section6" style="background-image: url('<?php echo content_url( 'uploads/2017/05/hold-on.png' ); ?>')">
			<div class="container cl-section4">
				<div class="row">
					<div class="col-md-12 text-center light-text ">
						<h1> { HOLD ON }</h1>
						<h2 class="align-text-c""><span class="album-title-color">&gt; <span class="text-u"><a href="https://www.youtube.com/watch?v=FtRfecRw4Qg" target="_blank" class="album-title-color">play music video</a></span> &lt;<a href="https://youtu.be/vK8b7uXm8ss" target="_blank" class="album-title-color"><br></a>&nbsp;</span></h2>
					</div>
				</div>
			</div>		
		</div>
		<div class="section body-color container" id="section5">
			<div id="section5-div" style="background-image: url('<?php echo content_url( 'uploads/2017/04/lyrics.jpg' ); ?>');">
				<div class="container lyrics">
					<div class="row">
						<div class="col-md-12 text-center light-text ">
							<h1> { LYRICS }</h1>
							<h2 class="align-text-c""><span class="album-title-color">&gt; <span class="text-u"><a href="/lyrics" target="_blank" class="album-title-color">click here</a></span> &lt;<a href="/lyrics" target="_blank" class="album-title-color"><br></a>&nbsp;</span></h2>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 tm-footer">
					<p class="text-center"><br>Copyright © Red Girl Records Ltd. <br><a href="/biography">Biography</a>&nbsp;|&nbsp;<a href="/contact" target="_self">Contact</a></p>
					<ul class="list-inline text-center icons">
						<li><a href="http://bit.ly/MelanieC_Twitter" target="_blank" title="Twitter"><img src="/wp-content/themes/default-theme/img/icons/twitter.png" alt="Twitter" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_Instagram" target="_blank" title="Instagram"><img src="/wp-content/themes/default-theme/img/icons/instagram.png" alt="Instagram" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_Facebook" target="_blank" title="Facebook"><img src="/wp-content/themes/default-theme/img/icons/fb.png" alt="Facebook" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_Youtube" target="_blank" title="YouTube"><img src="/wp-content/themes/default-theme/img/icons/yt.png" alt="YouTube" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_iTunes" target="_blank" title="iTunes"><img src="/wp-content/themes/default-theme/img/icons/itunes.png" alt="iTunes" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_Spotify" target="_blank" title="Spotify"><img src="/wp-content/themes/default-theme/img/icons/spotify.png" alt="Spotify" "=""></a></li>
						<li><a href="/news" target="_self" title="Newsletter"><img src="/wp-content/themes/default-theme/img/icons/email.png" alt="newsletter""></a></li>
						<li><a href="//store.melaniec.net" target="_blank" title="Store"><img src="/wp-content/themes/default-theme/img/icons/cart.png" alt="Store""></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>

<?php 
get_footer();
