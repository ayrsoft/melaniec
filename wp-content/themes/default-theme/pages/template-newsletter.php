<?php
/**
 * Template Name: Newsletter
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MelanieC
 *
 */


get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post();?>

			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h1><?php the_title(); ?></h1>
						<p style="color:#aeaeae">Written on <?php the_date('F j, Y'); ?></p>
						<div style="line-height: 2; font-size: 18px">
							<p><?php the_content(); ?></p>
						</div>
						<div class="form-group" style="background: white; padding: 25px;">
							<?php echo do_shortcode('[contact-form-7 id="83" title="Newsletter subscription"]'); ?>
						</div>
					</div>
				</div>
			</div>
			
			<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
if(! is_page('news')):
	get_sidebar();
endif;
get_footer();
