<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MelanieC
 */

?>
	<?php if ( ! is_front_page() && ! is_home() ):?>
			<div class="row">
				<div class="col-md-12 tm-footer">
					<p class="text-center"><br>Copyright © Red Girl Records Ltd. <br><a href="/biography">Biography</a>&nbsp;|&nbsp;<a href="/contact" target="_self">Contact</a></p>
					<ul class="list-inline text-center icons">
						<li><a href="http://bit.ly/MelanieC_Twitter" target="_blank" title="Twitter"><img src="/wp-content/themes/default-theme/img/icons/twitter.png" alt="Twitter" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_Instagram" target="_blank" title="Instagram"><img src="/wp-content/themes/default-theme/img/icons/instagram.png" alt="Instagram" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_Facebook" target="_blank" title="Facebook"><img src="/wp-content/themes/default-theme/img/icons/fb.png" alt="Facebook" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_Youtube" target="_blank" title="YouTube"><img src="/wp-content/themes/default-theme/img/icons/yt.png" alt="YouTube" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_iTunes" target="_blank" title="iTunes"><img src="/wp-content/themes/default-theme/img/icons/itunes.png" alt="iTunes" "=""></a></li>
						<li><a href="http://bit.ly/MelanieC_Spotify" target="_blank" title="Spotify"><img src="/wp-content/themes/default-theme/img/icons/spotify.png" alt="Spotify" "=""></a></li>
						<li><a href="/news/836-newsletter" target="_self" title="Newsletter"><img src="/wp-content/themes/default-theme/img/icons/email.png" alt="newsletter" "=""></a></li>
						<li><a href="http://store.melaniec.net" target="_blank" title="Store"><img src="/wp-content/themes/default-theme/img/icons/cart.png" alt="Store" "=""></a></li>
					</ul>
				</div>
			</div>
		</div><!-- #section -->
	<?php endif; ?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer none" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'default-theme' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'default-theme' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php printf( esc_html__( 'Theme: %1$s by %2$s.', 'default-theme' ), 'default-theme', '<a href="https://automattic.com/" rel="designer">Patricia Ayrah Otero</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
